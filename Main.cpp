#include <iostream>
#include "ObjectPtr.h"
#include "Child1.h"

int main()
{
	auto ptr = new Child1;
	ObjectPtr objectPtr(ptr);
	delete ptr;
	std::cout << objectPtr;

	return 0;
}

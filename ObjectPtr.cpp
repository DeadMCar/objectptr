﻿#include "ObjectPtr.h"
#include "ObjectArray.h"
#include "Object.h"


ObjectPtr::ObjectPtr(Object* ptr):ptr(ptr)
{
}


ObjectPtr::~ObjectPtr()
{
	if (get())
		delete get();
}


Object* ObjectPtr::get()
{
	return ObjectArray::get()->get(ptr);
}

ObjectPtr::operator Object* ()
{
	return get();
}

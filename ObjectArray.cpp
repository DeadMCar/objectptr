#include "ObjectArray.h"
#include "Object.h"
ObjectArray::ObjectArray()
{}

void ObjectArray::add(Object* ptr)
{
	arr.insert(ptr);
}

void ObjectArray::del(Object* ptr)
{
	arr.erase(ptr);
}

Object* ObjectArray::get(Object* ptr)
{
	auto i=arr.find(ptr);
	return (i == arr.end() ? nullptr : *i);
}

ObjectArray* ObjectArray::get()
{
	static ObjectArray* ptr = nullptr;
	if (ptr == nullptr)
	{
		ptr = new ObjectArray;
	}
	return ptr;
}


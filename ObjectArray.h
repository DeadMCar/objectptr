#pragma once
#include <set>
class Object;
class ObjectArray
{
	ObjectArray(const ObjectArray&) = delete;
	ObjectArray(ObjectArray&&) = delete;
private:
	ObjectArray();
	std::set<Object*> arr;
public:
	void add(Object*);
	void del(Object*);
	Object* get(Object*);
	static ObjectArray* get();
};
﻿#pragma once
#include <type_traits>
class Object;

class ObjectPtr
{
public:
	ObjectPtr(Object* ptr);
	~ObjectPtr();
	Object* get();
	operator Object*();
private:
	Object* ptr=nullptr;
};
